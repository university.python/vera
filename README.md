# Система версионирования ```vera```

Стройная система контроля версий, написанная на Python.

Мы работаем в терминах коммитов. Поддерживаем ветки и тэги.

## Пример использования

```bash
$ ./vera.sh init
$ ./vera.sh config -n author -v Georgiy

$ touch README.md .gitignore

$ ./vera.sh stage README.md .gitignore
$ ./vera.sh commit -m 'add basic project structure' 
```