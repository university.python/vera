from src.lib.commands import ConfigCommand


def parser_config(subparsers):
    parser = subparsers.add_parser('config', description='manipulate user preferences')

    parser.add_argument('-n', '--name', required=True)
    parser.add_argument('-v', '--value', required=False)

    parser.set_defaults(cls=ConfigCommand)
