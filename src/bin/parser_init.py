from src.lib.commands import InitCommand


def parser_init(subparsers):
    parser = subparsers.add_parser('init', description='initialize repository with $VERA_HOME name')

    parser.set_defaults(cls=InitCommand)
