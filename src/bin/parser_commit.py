from src.lib.commands import CommitCommand


def parser_commit(subparsers):
    parser = subparsers.add_parser('commit', description='create a commit with current staged files')

    parser.add_argument('-m', '--message', required=True)

    parser.set_defaults(cls=CommitCommand)
