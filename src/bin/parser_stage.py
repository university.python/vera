from src.lib.commands import StageCommand


def parser_stage(subparsers):
    parser = subparsers.add_parser('stage', description='stage file which will be included in commit')

    parser.add_argument('-r', '--remove', action='store_true', required=False, default=False)
    parser.add_argument('-l', '--list', action='store_true', required=False, default=False)
    parser.add_argument('filenames', nargs='*')

    parser.set_defaults(cls=StageCommand)
