from src.lib.filesystem import FileSystem
from src.lib.objects import Objects


class Blob(Objects):
    def __init__(self, content):
        self.content = content

    def type(self) -> str:
        return 'blob'

    def to_lines(self, filesystem: FileSystem):
        yield from self.content

    @staticmethod
    def from_lines(content):
        return Blob(content)
