from abc import abstractmethod, ABC

from src.lib.filesystem import FileSystem


class Objects(ABC):
    @abstractmethod
    def type(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def to_lines(self, filesystem: FileSystem):
        raise NotImplementedError()

    @staticmethod
    def from_lines(self, content):
        raise NotImplementedError()
