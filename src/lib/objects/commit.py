import json

from src.lib.filesystem import FileSystem, ObjectSystem
from src.lib.objects import Objects, Tree


class Commit(Objects):
    def __init__(self, tree: Tree, author: str, message: str, previous=None):
        self.tree = tree
        self.author = author
        self.message = message
        self.previous = previous

    def type(self) -> str:
        return 'commit'

    def to_lines(self, filesystem: FileSystem):
        object_system = ObjectSystem(filesystem)

        tree_hash = object_system.store_object(self.tree)

        representation = {'author': self.author, 'message': self.message, 'tree': tree_hash, 'previous': self.previous}

        yield json.dumps(representation)

    @staticmethod
    def from_lines(content):
        representation = json.loads(''.join(content))

        author = representation['author']
        tree = representation['tree']
        message = representation['message']
        previous = representation['previous']

        return Commit(tree, author, message, previous)
