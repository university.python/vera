import json

from src.lib.filesystem import FileSystem, ObjectSystem
from src.lib.objects import Objects, Blob


class Tree(Objects):
    def __init__(self, files):
        self.files = files

    def type(self) -> str:
        return 'tree'

    def to_lines(self, filesystem: FileSystem):
        object_system = ObjectSystem(filesystem)

        path_to_hash_map = dict()

        for path, file in self.files:
            blob = Blob(file)
            file_hash = object_system.store_object(blob)

            path_to_hash_map[path] = file_hash

        yield json.dumps(path_to_hash_map)

    @staticmethod
    def from_lines(content):
        content = ''.join(content)
        representation = json.loads(content)

        files = representation.items()

        return Tree(files)