import os


class FileSystemError(RuntimeError):
    pass


class FileSystem:
    def __init__(self, base_folder: str):
        self.__base_folder = base_folder

    def base_folder(self):
        return self.__base_folder

    def folders(self):
        return [self.base_folder(), self.objects_folder(), self.branches_folder(), self.tags_folder(),
                self.staged_folder(), self.config_file()]

    def exists(self, path):
        return os.path.exists(path)

    def check(self):
        for folder in self.folders():
            if not self.exists(folder):
                raise FileSystemError('incorrect folder structure')

    def sub(self, path, base):
        return os.path.join(base, path)

    def objects_folder(self):
        return self.sub('objects', self.base_folder())

    def staged_folder(self):
        return self.sub('staged', self.base_folder())

    def branches_folder(self):
        return self.sub('branches', self.base_folder())

    def tags_folder(self):
        return self.sub('tags', self.base_folder())

    def config_file(self):
        return self.sub('config.json', self.base_folder())

    def read(self, path):
        with open(path, 'r') as file:
            yield from file

    def __is_subfolder(self, path):
        base_folder = os.path.abspath(self.base_folder())
        path = os.path.abspath(path)

        return path.startswith(base_folder)

    def remove(self, path):
        if not self.__is_subfolder(path):
            raise FileSystemError('cannot change anything outside base folder')

        if not self.exists(path):
            raise FileNotFoundError(f'trying to remove nonexisting file {path}')

        os.remove(path)

    def list(self, path):
        yield from os.scandir(path)

    def write(self, path, content):
        if not self.__is_subfolder(path):
            raise FileSystemError(f'cannot change {path} outside base folder')

        with open(path, 'w', encoding='utf8') as file:
            file.writelines(content)
