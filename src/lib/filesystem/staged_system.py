import os.path
from hashlib import sha1
from itertools import chain

from src.lib.filesystem import FileSystem


class StagedSystem:
    def __init__(self, filesystem: FileSystem):
        self.filesystem = filesystem

    def path(self, filename):
        path = self.filesystem.staged_folder()
        path = self.filesystem.sub(filename, path)

        return path

    def __staged(self, path):
        path_bytes = path.encode('utf8')
        path_hash = sha1(path_bytes).hexdigest()

        return self.path(path_hash)

    def add(self, path):
        relative_path = os.path.relpath(path)

        content = self.filesystem.read(relative_path)
        content = chain([f'{relative_path}\n'], content)

        staged_path = self.__staged(relative_path)

        self.filesystem.write(staged_path, content)

    def remove(self, path):
        relative_path = os.path.relpath(path)
        staged_path = self.__staged(relative_path)

        self.filesystem.remove(staged_path)

    def get_staged_content(self):
        for filename in self.filesystem.list(self.filesystem.staged_folder()):
            content = self.filesystem.read(filename)
            path = next(content).strip()

            yield path, content

    def list_staged_files(self):
        for filename in self.filesystem.list(self.filesystem.staged_folder()):
            content = self.filesystem.read(filename)
            path = next(content).strip()

            yield path

    def clear(self):
        for filename in self.filesystem.list(self.filesystem.staged_folder()):
            self.filesystem.remove(filename)
