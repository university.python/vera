import json

from src.lib.filesystem import FileSystem, FileSystemError


class ConfigSystem:
    def __init__(self, filesystem: FileSystem):
        self.filesystem = filesystem

    def get_path(self):
        return self.filesystem.config_file()

    def exists(self):
        path = self.get_path()

        return self.filesystem.exists(path)

    def __write(self, content):
        path = self.get_path()

        self.filesystem.write(path, content)

    def get_config(self) -> dict:
        if not self.exists():
            raise FileSystemError(f'there is no config')

        path = self.get_path()
        content = self.filesystem.read(path)
        content = ''.join(content)

        config = json.loads(content)

        return config

    def create_config(self, config):
        if self.exists():
            raise FileSystemError(f'config already exists')

        content = json.dumps(config)

        self.__write(content)

    def update_config(self, config):
        if not self.exists():
            raise FileSystemError(f'cannot update nonexisting config')

        content = json.dumps(config)

        self.__write(content)

    def set_value(self, **kwargs):
        config = self.get_config()

        for key, value in kwargs.items():
            config[key] = value

        self.update_config(config)
