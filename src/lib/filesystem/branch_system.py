from src.lib.filesystem import FileSystem, FileSystemError


class BranchSystem:
    def __init__(self, filesystem: FileSystem):
        self.filesystem = filesystem

    def get_path(self, branch_name: str):
        path = self.filesystem.base_folder()
        path = self.filesystem.sub(branch_name, path)

        return path

    def exists(self, branch_name: str):
        path = self.get_path(branch_name)

        return self.filesystem.exists(path)

    def __write(self, branch_name: str, commit_hash: str):
        path = self.get_path(branch_name)

        self.filesystem.write(path, commit_hash)

    def get_commit(self, branch_name: str) -> str:
        if not self.exists(branch_name):
            raise FileSystemError(f'cannot find branch {branch_name}')

        path = self.get_path(branch_name)
        content = self.filesystem.read(path)

        first_line = next(content)

        return first_line

    def create_branch(self, branch_name: str, commit_hash: str):
        if self.exists(branch_name):
            raise FileSystemError(f'branch {branch_name} already exists')

        self.__write(branch_name, commit_hash)

    def update_branch(self, branch_name: str, commit_hash: str):
        if not self.exists(branch_name):
            raise FileSystemError(f'cannot update nonexisting branch {branch_name}')

        self.__write(branch_name, commit_hash)
