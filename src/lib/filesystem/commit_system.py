from src.lib.filesystem import FileSystem, FileSystemError, ObjectSystem
from src.lib.objects import Commit


class CommitSystem:
    def __init__(self, filesystem: FileSystem):
        self.filesystem = filesystem

    def commit_iterator(self, commit_hash_start: str):
        object_system = ObjectSystem(self.filesystem)

        object_type, object_hash, object_content = object_system.get_object(commit_hash_start)

        if object_type != 'commit':
            return

        commit = Commit.from_lines(object_content)

        print(commit) # TODO

