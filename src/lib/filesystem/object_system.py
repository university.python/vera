import os.path
from hashlib import sha1
from itertools import chain

from src.lib.filesystem import FileSystem, FileSystemError


class ObjectSystem:
    def __init__(self, filesystem: FileSystem):
        self.filesystem = filesystem

    def get_path(self, object_name):
        path = self.filesystem.objects_folder()
        path = self.filesystem.sub(object_name, path)

        return path

    def exists(self, object_name):
        path = self.get_path(object_name)

        return os.path.exists(path)

    def __write(self, object_name, content):
        if self.exists(object_name):
            raise FileSystemError(f'cannot recreate object {object_name}')

        path = self.get_path(object_name)

        self.filesystem.write(path, content)

    def __list(self):
        folder = self.filesystem.objects_folder()

        for file in os.scandir(folder):
            yield file.name

    @staticmethod
    def __hash(line: str) -> str:
        line_bytes = line.encode('utf8')
        line_hash = sha1(line_bytes)

        return line_hash.hexdigest()

    def __generate_hash(self, object_type, object_content):
        line = next(object_content)
        object_content = chain([line], object_content)

        object_hash = ObjectSystem.__hash(line)

        if self.exists(object_hash):
            content_lines = list(object_content)

            while self.exists(object_hash):
                other_type, other_hash, other_content = self.get_object(object_hash)

                if object_type == other_type:
                    other_lines = list(other_content)

                    if content_lines == other_lines:
                        break

                object_hash = ObjectSystem.__hash(line + object_hash)

            object_content = iter(content_lines)

        return object_type, object_hash, object_content

    def store_object(self, object_):
        object_type = object_.type()
        object_content = object_.to_lines(self.filesystem)

        object_type, object_hash, object_content = self.__generate_hash(object_type, object_content)

        header = f'{object_type} {object_hash}\n'

        object_content = chain([header], object_content)

        if not self.exists(object_hash):
            self.__write(object_hash, object_content)

        return object_hash

    def get_filename(self, hash_start):
        object_filename = None

        for filename in self.__list():
            if filename.startswith(hash_start):
                object_filename = filename
                break

        if object_filename is None:
            raise FileSystemError(f'cannot find file with cache {hash_start}')

        return object_filename

    def get_object(self, hash_start: str):
        object_filename = self.get_filename(hash_start)
        object_content = self.filesystem.read(object_filename)

        info = next(object_content)
        object_type, object_hash = info.split(' ')

        if object_hash != object_filename:
            raise FileSystemError(f'file {object_filename} has unmached hashes')

        return object_type, object_hash, object_content
