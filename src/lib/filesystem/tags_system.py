from src.lib.filesystem import FileSystem, FileSystemError


class TagSystem:
    def __init__(self, filesystem: FileSystem):
        self.filesystem = filesystem

    def path(self, tag):
        path = self.filesystem.tags_folder()
        path = self.filesystem.sub(tag, path)

        return path

    def exists(self, tag):
        path = self.path(tag)

        return self.filesystem.exists(path)

    def __write(self, tag, commit_hash):
        path = self.path(tag)

        self.filesystem.write(path, commit_hash)

    def get_commit(self, tag):
        if not self.exists(tag):
            raise FileSystemError(f'cannot find tag {tag}')

        content = self.filesystem.read(self.path(tag))
        first_line = next(content)

        return first_line

    def create_tag(self, tag, commit_hash):
        if self.exists(tag):
            raise FileSystemError(f'tag {tag} already exists')

        self.__write(tag, commit_hash)

    def update_tag(self, tag, commit_hash):
        if not self.exists(tag):
            raise FileSystemError(f'cannot update nonexisting tag {tag}')

        self.__write(tag, commit_hash)
