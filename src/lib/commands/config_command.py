from src.lib.commands import Command
from src.lib.filesystem import FileSystem, ConfigSystem


class ConfigCommand(Command):
    def execute(self, filesystem: FileSystem, args) -> None:
        config_system = ConfigSystem(filesystem)

        if args.value is None:
            print(config_system.get_config().get(args.name, 'parameter is unset'))

            return

        parameter = dict()
        parameter[args.name] = args.value
        config_system.set_value(**parameter)
