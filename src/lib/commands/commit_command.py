from src.lib.commands import Command
from src.lib.filesystem import FileSystem, StagedSystem, ObjectSystem, ConfigSystem, FileSystemError
from src.lib.objects import Tree, Commit


class CommitCommand(Command):
    def execute(self, filesystem: FileSystem, args) -> None:
        staged_system = StagedSystem(filesystem)

        tree = Tree(staged_system.get_staged_content())

        config_system = ConfigSystem(filesystem)
        config = config_system.get_config()

        if 'author' not in config.keys():
            raise FileSystemError('you should provide username: vera config -n author -v <name>')

        commit = Commit(tree, config.get('author'), args.message, config.get('last_commit'))

        object_system = ObjectSystem(filesystem)

        commit_hash = object_system.store_object(commit)

        config_system.set_value(last_commit=commit_hash)

        staged_system.clear()
