from abc import ABC, abstractmethod

from src.lib.filesystem import FileSystem


class Command(ABC):
    @abstractmethod
    def execute(self, filesystem: FileSystem, args) -> None:
        raise NotImplementedError()
