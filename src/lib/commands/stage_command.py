from src.lib.commands import Command
from src.lib.filesystem import FileSystem, StagedSystem


class StageCommand(Command):
    def execute(self, filesystem: FileSystem, args) -> None:
        filesystem.check()

        staged_system = StagedSystem(filesystem)

        action = staged_system.remove if args.remove else staged_system.add

        for file in args.filenames:
            action(file)

        if args.list:
            for file in staged_system.list_staged_files():
                print(file)
