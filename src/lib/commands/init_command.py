import json
import os

from src.lib.commands import Command
from src.lib.filesystem import FileSystem


class InitCommand(Command):
    def __init__(self):
        self.home_folder = os.environ.get('VERA_HOME', '.vera')

    def execute(self, filesystem: FileSystem, args) -> None:
        if os.path.isdir(self.home_folder):
            return

        self.create_folder_structure(filesystem)
        self.create_config(filesystem)

    def create_folder_structure(self, filesystem: FileSystem):
        folder_list = [filesystem.base_folder(), filesystem.objects_folder(), filesystem.staged_folder(),
                       filesystem.branches_folder(), filesystem.tags_folder(), ]

        for folder in folder_list:
            os.makedirs(folder, exist_ok=True)

    def create_config(self, filesystem: FileSystem):
        default_config = {'branch': None}

        filesystem.write(filesystem.config_file(), json.dumps(default_config))
