import argparse
import os
import sys

from src.bin import *
from src.lib.filesystem import FileSystem


def parse_args():
    parser = argparse.ArgumentParser(
        prog='Vera', description='Simple version control system')

    subparsers = parser.add_subparsers(required=True)

    parser_init(subparsers)
    parser_stage(subparsers)
    parser_config(subparsers)
    parser_commit(subparsers)

    return parser.parse_args()


def main():
    args = parse_args()

    vera_home = os.environ.get('VERA_HOME', '.vera')
    filesystem = FileSystem(vera_home)

    # try:
    args.cls().execute(filesystem, args)
    # except Exception as e:
    #     print(e.args[0], file=sys.stderr)


if __name__ == '__main__':
    main()
